import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import 'antd/dist/antd.css';

import { Layout, Card, Progress, Row, Col } from 'antd';
const { Content } = Layout

import L2eTimeLine from '../pages/components/L2eTimeLine'
import L2eCollapse from '../pages/components/L2eCollapse'

import L2ePage from '../pages/components/L2ePage'
import L2eProgress from '../pages/components/L2eProgress'
import L2eBuyButton from '../pages/components/L2eBuyButton'
import L2eCardPanel from '../pages/components/L2eCardPanel';

storiesOf("L2e pages", module)
  .add("L2E", () =>
    <L2ePage>
      <Content />
      <L2eCardPanel></L2eCardPanel>
      <L2eTimeLine />
    </L2ePage>
  )
  .add("Pre Sell", () =>
    <L2ePage>

      <L2eProgress
        title="First Round"
        percent={100}
        date="2018.8 - 2018.10"
      >
      </L2eProgress>

      <L2eProgress
        title="Second Round"
        percent={50}
        date="2018.8 - 2018.10"
      >
      </L2eProgress>

      <L2eProgress
        title="Third Round"
        percent={0}
        date="2018.8 - 2018.10"
      >
      </L2eProgress>

    </L2ePage>
  )
  .add("FAQ", () =>
    <L2ePage>
      <L2eCollapse />
    </L2ePage>
  )
