import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import 'antd/dist/antd.css';

import { Layout } from 'antd';
import L2eHeader from '../pages/components/L2eHeader'
import L2eTimeLine from '../pages/components/L2eTimeLine'
import L2eFooter from '../pages/components/L2eFooter'
import L2eCollapse from '../pages/components/L2eCollapse'
import L2eCarousel from '../pages/components/L2eCarousel'
import L2eCardPanel from '../pages/components/L2eCardPanel';

storiesOf("L2e components", module)
  .add("header", () =>
    <Layout>
      <L2eHeader pages={{}} links={{}} onClick={action('clicked')} />
    </Layout>
  )
  .add("time line", () =>
    <L2eTimeLine>
    </L2eTimeLine>
  )
  .add("footer", () =>
    <L2eFooter>
    </L2eFooter>
  )
  .add("collapse", () =>
    <L2eCollapse>
    </L2eCollapse>
  )
  .add("carousel", () =>
    <L2eCarousel>
    </L2eCarousel>
).add("card panel", () => 
  <L2eCardPanel>
  </L2eCardPanel>
)
