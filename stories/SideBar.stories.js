import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import 'antd/dist/antd.css';

import SideBar from '../pages/components/SideBar'
import { Layout, Breadcrumb, } from 'antd';
const { Header, Content, Footer } = Layout;

storiesOf("SideBar", module)
  .add("default preview", () => (
    <Layout style={{ minHeight: '100vh' }}>
      <SideBar
        onClick={action('clicked')}
        items={[
          { key: "chart", icon: "pie-chart", name: "Chart" },
          { key: "desktop", icon: "desktop", name: "Desktop" },
          {
            key: "user", icon: "user", name: "User",
            subs: [
              { key: "tom", name: "Tom" },
              { key: "bill", name: "Bill" },
              { key: "alex", name: "Alex" },
            ]
          },
          {
            key: "team", icon: "team", name: "Team",
            subs: [
              { key: "team1", name: "Team 1" },
              { key: "team2", name: "Team 2" },
              { key: "team3", name: "Team 3" },
            ]
          },
          { key: "file", icon: "file", name: "File" },
        ]}
      />
      <Layout>
        <Header style={{ textAlign: 'center', background: '#fff', padding: 0 }} >
          Header text
          </Header>
        <Content style={{ margin: '0 16px' }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>User</Breadcrumb.Item>
            <Breadcrumb.Item>Bill</Breadcrumb.Item>
          </Breadcrumb>
          <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
            test text.
            </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          Wechat-Assist Monitor ©2018 Created by Bai
          </Footer>
      </Layout>
    </Layout>
  ))