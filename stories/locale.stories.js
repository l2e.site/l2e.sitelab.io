import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import 'antd/dist/antd.css';

import Language from '../pages/components/Language'
import { Pagination } from 'antd';

import { IntlProvider, addLocaleData, FormattedMessage } from 'react-intl';

import zh_CN from '../locale/zh'
import en_US from '../locale/en'

storiesOf("locale", module)
  .add("en & zh", () => 
    <Language >
      <div>
        <Pagination defaultCurrent={1} total={50} showSizeChanger />
      </div>
    </Language>
  )
  .add("intl zh", () =>
    <IntlProvider locale='en' messages={zh_CN}>
      <div>
        <FormattedMessage
          id="name"
          values={{ name: <b> 某某某 </b> }}
        />
      </div>
    </IntlProvider>
  )
  .add("intl en", () =>
    <IntlProvider locale='en' messages={en_US}>
      <div>
        <FormattedMessage
          id="name"
          values={{ name: <b> 某某某 </b> }}
        />
        <p>
          {navigator.language}
        </p>
      </div>
    </IntlProvider>
  )