function assert(cond, msg) {
  if (!cond) throw new Error('l2eToken.js: ' + msg)
}

export default class L2eToken {

  constructor(web3, instance) {
    this.web3 = web3
    this.instance = instance
  }

  update(arr) {
    let ps = arr.map(e => {
      if (typeof e == 'string') return [e]
      else if (Array.isArray(e)) return e
      else assert(false, 'update(arr: string | Array)')
    }).map(cmd => {
      let method = cmd[0]
      assert(method, 'this method does not exist')
      return new Promise((resolve, reject) => {
        let cb = (err, ret) => {
          if (err) reject(err)
          else resolve(ret)
        }
        this.instance[method].apply(
          this.instance,
          [...cmd.slice(1), cb]
        )
      })
    })
    return Promise.all(ps)
  }

  get totalSupply() {
    assert(this._totalSupply, 'not update totalSupply')
    return this.web3.fromWei(this._totalSupply, 'finney').toString()
  }

  get state() {
    assert(this._state, "not update state")
    return this._state
  }

  get firstRoundRate() {
    assert(this._firstRoundRate, "not update firstRoundRate")
    return this._firstRoundRate.toString()
  }

  get secondRoundRate() {
    assert(this._secondRoundRate, "not update secondRoundRate")
    return this._secondRoundRate.toString()
  }

  get firstRoundAmount() {
    assert(this._firstRoundAmount, "not update firstRoundAmount")
    return this.web3.fromWei(this._firstRoundAmount, 'finney').toString()
  }

  get mainnetCount() {
    assert(this._mainnetCount, "not update mainnetCount")
    return this._mainnetCount.toString()
  }

  get pendingCount() {
    assert(this._pendingCount, "not update pendingCount")
    return this._pendingCount.toString()
  }

  /*
  SaleState public state = SaleState.FirstRound;
  uint256 public firstRoundRate; // 100 means 0.001 ether, or 1 finney
  uint256 public secondRoundRate;
  uint256 public firstRoundAmount = (2.5 * 10**6) * 1 finney;
  uint256 public mainnetCount;
  uint256 public pendingCount;
  */

  balanceOf(address) {
    let key = '_balanceOf,' + address
    assert(this[key], 'not update ' + key)
    return this.web3.fromWei(this[key], 'finney').toString()
  }

  getSelled() {
    return this.totalSupply - this.balanceOf('0x0')
  }

  getRest() {
    return this.balanceOf('0x0')
  }

  getFirstRoundPriceOf(n) {
    return n * (this.firstRoundRate / 100) / 1000
  }

  getSecondRoundPriceOf(n) {
    return n * (this.secondRoundRate / 100) / 1000
  }
}