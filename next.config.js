// next.config.js
module.exports = {

  exportPathMap: function () {
    return {
      "/": { page: "/" },
      "/faq": { page: "/faq" },
      "/pre-sell": { page: "/pre-sell" },
    }
  },

  assetPrefix: process.env.ENV === "production" ? "/l2e-me" : "",
}