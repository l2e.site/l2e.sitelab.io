import React from 'react'

import L2ePage from './components/L2ePage'
import L2eTimeLine from './components/L2eTimeLine'
import L2eCardPanel from './components/L2eCardPanel';

export default class Page extends React.Component {

  static async getInitialProps () {
    return {}
  }

  render() {
    // console.log(this.props.url)
    return (
      <L2ePage>
        <L2eCardPanel />
        <L2eTimeLine />
      </L2ePage>
    );
  }
}