import React from 'react'

import L2ePage from '../components/L2ePage'
import L2eCollapse from '../components/L2eCollapse'

export default function FAQ () {
  return (
    <L2ePage>
      <L2eCollapse />
    </L2ePage>
  )
}

FAQ.getInitialProps = async () => {
  return {}
}