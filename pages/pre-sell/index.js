import React from 'react'

import Head from 'next/head'
import L2ePage from '../components/L2ePage'
import L2eProgress from '../components/L2eProgress'

import abi from '../../lib/abi.json'
import getWeb3 from '../../lib/getWeb3'
import L2eToken from '../../lib/L2eToken'

/**
 * @type {L2eToken}
 */
let l2e;

export default class PreSell extends React.Component {

  static async getInitialProps () {
    return {}
  }

  constructor(props) {
    super(props)
    this.state = {

    }
  }

  componentWillMount() {
    getWeb3()
      .then(({ web3 }) => {
        l2e = new L2eToken(web3, web3.eth.contract(abi).at("0x0d5195906c2093ba8994066604bc10a7eb69beee"))
      })
      .then(() => console.log("web3 mount ..."))
  }

  render() {
    // console.log(this.props.url)
    return (
      <L2ePage>
        <Head>
          <script src="https://cdn.jsdelivr.net/gh/ethereum/web3.js/dist/web3.min.js"></script>
          <meta key="AppHead" />
        </Head>

        <L2eProgress
          title="First Round"
          percent={100}
          date="2018.8 - 2018.10"
        >
        </L2eProgress>

        <L2eProgress
          title="Second Round"
          percent={50}
          date="2018.8 - 2018.10"
        >
        </L2eProgress>

        <L2eProgress
          title="Third Round"
          percent={0}
          date="2018.8 - 2018.10"
        >
        </L2eProgress>
      </L2ePage>
    );
  }
}