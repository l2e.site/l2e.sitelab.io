import React from 'react'

import Card from 'antd/lib/card'
import Modal from 'antd/lib/modal'

import pick from 'lodash/pick'

function CardAndModal({cardObj}) {
  return (
      <Card
        hoverable
        style={{ margin: '20px' }}
        title={cardObj.title}
        onClick={() => Modal.info(pick(cardObj, ['title', 'content']))}
      >{cardObj.abstract}</Card>
  )
}

const tabList = [{
  tab: 'The Most important!!',
  key: 'tmi',
}, {
  tab: 'The Second Question!',
  key: 'tsq',
}];

const contentList = {
  tmi: <d>
    <p>所有数字货币在取代法币的道路上，都面临着一个巨大的挑战——<b>“值多少美元？”</b></p>
    <p>什么意思呢？当我们要支付一种数字货币的时候，你能买到价值多少钱的东西呢？答案是——<b>“和当前币价兑换法币的价值相等的东西。”</b></p>
    <br/>
    <p>是的，L2E项目正是为此而生的一个大型试验。</p>
    <p>我们致力于引入一种新的量度——<b>“用户时间”</b>，并在此基础上构建一种新型的共识网络。</p>
  </d>,
  tsq:<d>
    <p>在数字货币网络中占有较大份额的人，会自发地维护网络价值？——<b>NO！货币价值剧烈波动才是他们的利益所在。</b> </p>
    <p>我们一直警惕法币的恣意超发，<b>我们却为何对这些陌生的巨头如此宽容？ </b></p>
    <br/>
    <p>于是乎，我们L2E项目尝试给出一个新的答案。</p>
    <p>1）占有较大挖矿权重之人必将承担长期的币价波动。</p>
    <p>2）矿池给自己挖矿只能维持自己在网络中的权重，换言之给自己挖矿不产生新的价值，只有越来越多的人通过这个矿池向其他人支付“用户时间”才能使矿池利益最大化。</p>
  </d>
};

const tabListNoTitle = [{
  tab: 'PoU 共识算法',
  key: 'article',
}, {
  tab: 'Shadow Chain 影子链',
  key: 'app',
}, {
  tab: 'EGO 脚本',
  key: 'project',
}];

const contentListNoTitle = {
  article: <p>Proof-of-Usertime （PoU）共识算法，是我们独创的PoS/PoB的变种算法。该算法的主要特征是，可以积累最多一天的挖矿权重，并一次性转移给其他任意地址。该过程完全在链下执行，绝对的去中心化，去信任化。

  该过程秒支付，无需信任——发送一段时间的签名给“电站”。
  
  代币可以转换为“发电机”（该过程不可逆），“发电机”每天可以积累一定数量的“挖矿权重”称为“电池”。用户可以自己使用这个挖矿权重给自己挖代币，或者可以通过“电站”将“电池”转移给其他任意地址。“发电机”使用8～10年后过期。
  
  该共识算法鼓励支付“虚拟算力”而不是直接支付代币，这样支付方和接收方，的预期都会稳定在很长一段时间的平均值。例如：曾经有人用一万个比特币买了两个披萨，虚拟货币的币值相对法比波动是必然的现象。然而，这将导致支付方和接收方预期的不一致，如果在币价偏低时，接收方乐意接受支付，但是消费者不一定愿意支付。反之，消费者乐意在币价较高时支付，而接收方则不愿意。如果在前文所述的例子中，这个人没有支付掉一万个比特币，而是支付掉了他所有的矿机几天时间的“算力”，那么在十多年，他的矿机（代表了他在比特币网络中所占的权重）依旧存在，而且当时他也支付了披萨。并且，在卖方看来，他也支付了当时对等的算力（有用电的成本）。如果是这样的话，买方卖方的预期都不会受剧烈波动的币价影响。L2E项目的PoU共识算法的灵感正是来源于此，并且选用PoB（Proof-of-Burn）将此过程虚拟化。</p>,
  app: <p>请想象这样一个场景，在比特币的网络中，每时每刻，都有茫茫多的数据被计算出Hash值。我们虽然在区块链上见不到全部的这些数据，我们也无法收集全他们，但是我们却都知道它们真真实实地存在着，并且在幕后支撑着这个网络的安全性。

  L2E的影子链的灵感正是来源于此。使用PoU支付时，所转移的“虚拟算力（电池）”，可以在一段时间之后，连接到一个当前最新的“虚拟算力（电池）”上，形成一条跨越了一段时间的短链，短链最多能连接3个时间点。如果有网络破坏者，想在多数用户不知情的情况下，偷偷挖一条分叉的链，则他的算力将直接降至原本算力的1/3 ～ 1/2。</p>,
  project: <p>根据L2E项目特点，我们基于ChezScheme编写了一种Lisp方言作为智能合约的脚本语言——EGO脚本。Lisp语言的“数据即是代码”的元编程能力，能很好的运用在L2E项目的UTXO结构中，这将使L2E的智能锁合约具有近乎无限的灵活性。

  嵌入Lisp脚本，是因为UTXO本身和函数式很像，结合紧密。 并且使用Lisp语言代码生成代码，完美满足UTXO输入是代码数据，按照输入运行，输出是代码数据的需求。</p>,
};

export default class L2eCardPanel extends React.Component {

  state = {

    test: {
      title: <h1>Details</h1>,
      abstract: "asdf",
      content: <d>
        <p>a...</p>
        <p>a...</p>
        <p>a...</p>
      </d>,
    },

    test1: {
      title: <h1>DetailsDetailsDetailsDetailsDetailsDetailsDetails</h1>,
      abstract: "asdf",
      content: <d>
        <p>a...</p>
        <p>a...</p>
        <p>a...</p>
      </d>,
    },

    key: 'tmi',
    noTitleKey: 'app',
  }

  onTabChange = (key, type) => {
    console.log(key, type);
    this.setState({ [type]: key });
  }

  render() {

    return (
      <div style={{ padding: '16px' }}>
        <Card
          hoverable
          style={{ width: '100%' }}
          title={<h2>L2E Project</h2>}
          extra={<a href="#">by L2E Team</a>}
          tabList={tabList}
          activeTabKey={this.state.key}
          onTabChange={(key) => { this.onTabChange(key, 'key'); }}
        >
          {contentList[this.state.key]}
        </Card>
        <br /><br />
        <Card
          hoverable
          style={{ width: '100%' }}
          tabList={tabListNoTitle}
          activeTabKey={this.state.noTitleKey}
          onTabChange={(key) => { this.onTabChange(key, 'noTitleKey'); }}
        >
          {contentListNoTitle[this.state.noTitleKey]}
        </Card>
      </div>
    )
  }

}