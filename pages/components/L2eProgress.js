import React from 'react'

import Progress from 'antd/lib/progress'
import Card from 'antd/lib/card'

import { Describe } from './L2eHint'
import L2eBuyButton from './L2eBuyButton'

// state: "onSell" | "done" | "notStart"
export default function L2eProgress ({ title, state, percent, date }) {

  return (
    <Card
      hoverable
      style={{ marginTop: 16 }}
      extra={<L2eBuyButton />}
      title={title}
    >
      <Describe content="progress hint">
        <span>{date}</span>
        <Progress percent={percent} />
      </Describe>
    </Card>
  )
}