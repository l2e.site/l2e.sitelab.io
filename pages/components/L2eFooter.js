import React from 'react';

import Layout from 'antd/lib/layout'
const { Footer } = Layout;

export default class L2eFooter extends React.Component {

  render() {
    return (
      <Footer >
        <p> L2E Project Website ©2018 Created by <a href="https://github.com/l2eproject" target="_blanck">L2E Team</a> </p>
        <p>Donate BTC: 0x000000</p>
        <p>Donate ETH: 0x000000</p>
        <p>Donate XMR: 0x000000</p>
      </Footer>
    )
  }
}