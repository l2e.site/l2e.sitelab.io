import React from 'react';

import Collapse from 'antd/lib/collapse'

const CreateContentItem = (content) =>
  <Collapse.Panel header={content[1]} key={content[0]}>
    <p>{content[2]}</p>
  </Collapse.Panel>

export default function L2eCollapse({contents}) {
  return (
    <Collapse onChange={() => {}}>
      {contents.map(CreateContentItem)}
    </Collapse>
  )
}

L2eCollapse.defaultProps = {
  contents: [
    ["1", "电站其实是一个矿池的概念，作为矿池，如何获取收益？",
    <d>
    有两种情况，一种是内容服务商自己建立电站（矿池），接受用户支付的算力之后，自己挖矿给自己。
    </d>
    ],
    ["2", "若内容提供商提供微内容，如何公平的获取相应收益？",
     "支付算力的特性，专为内容服务商高频率，快速接受用户的支付设计。如果只是比较小的交易，可以直接用发送代币的形式。"
    ],
    ["3", "Ego除了实现了之前比特币智能合约可以实现的智能合约，多重签名，是否完成了以太坊以下功能？",
     "Ego是scheme语言的超集，是图灵完全的语言。多重签名功能可以完全由编写脚本实现。"
    ],
    ["4", "通货膨胀的数字货币如何进行分配？", 
    "全网公平地挖出来。"
    ],
    ["5", "This is panel header", "reserved"],
    ["6", "This is panel header", "reserved"],
    ["7", "This is panel header", "reserved"],
  ]
}