import React from 'react'

import Layout from 'antd/lib/layout'
import BackTop from 'antd/lib/back-top'
const { Content, } = Layout;

import Head from 'next/head';

import L2eHeader from './L2eHeader'
import L2eFooter from './L2eFooter'

export default class L2ePage extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Head>
          <link rel='stylesheet' href="https://cdn.bootcss.com/antd/3.5.4/antd.css" />
          <meta key="AppHead" />
        </Head>
        <L2eHeader />
        <Content style={{ padding: '0 50px', marginTop: 64 }}>
          <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
            {this.props.children}
          </div>
          <BackTop />
        </Content>
        <L2eFooter />
      </Layout>
    )
  }
}