import React from 'react'
import Timeline from 'antd/lib/timeline'
import Card from 'antd/lib/card'
import Icon from 'antd/lib/icon'

/**
 * choiceIcon by current state
 *
 * @param {"done"|"doing"|"planning"|null} state
 */
function choiceIcon(state) {
  switch(state) {
    case "done": return "check-circle";
    case "doing": return "loading";
    case "planning": return "check-circle-o";
    default: return "question-circle-o";
  }
}

let itemCount = 0

const CreateItem = (state, title, body) =>
  <Timeline.Item key={itemCount++} color={state === "done" ? "green" : "gray"}>
    <Card
      hoverable
      title={
        <span>
          <Icon type={choiceIcon(state)}
            style={{ marginRight: "10px" }}
          />
          <span>{title}</span>
        </span>
      }
    >
      {body}
    </Card>
  </Timeline.Item>

export default class L2eTimeLine extends React.Component {

  static defaultProps = {
    items: [
      ["doing", "2018.09.01, l2e project start", [
        <p>项目准备阶段</p>
      ]],
      ["doing", "2018.10.01, website & SNS", [
        <p key="1">正式的官方主页，和社交网络帐号的运营</p>,
      ]],
      ["done", "2018.12.01, white paper & pre-sell", [
        <p key="2">发布技术白皮书，开始预售一定数量的代币</p>,
      ]],
      ["planning", "2019.01.15, test net", [
        <p key="2">测试网络</p>,
      ]],
      [null, "2019.03.15, reserved", []],
    ]
  }

  render() {
    return (
      <Timeline pending="Recording..." style={{ margin: "20px" }}>
        {this.props.items.map(e => CreateItem.apply(null, e))}
      </Timeline>
    )
  }
}