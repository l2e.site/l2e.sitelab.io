import React from 'react';

import Router from 'next/router'
import Layout from 'antd/lib/layout'
import Menu from 'antd/lib/menu'
import Icon from 'antd/lib/icon'
const { Header } = Layout;

export default class L2eHeader extends React.Component {

  static defaultProps = {
    // direct to
    pages: {
      "index": "/",
      "pre-sell": "/pre-sell",
      "faq": "/faq",
    },
    links: {
      // faq: "https://github.com/l2eproject/l2eproject.github.io/wiki/FAQ",
      github: "https://github.com/l2eproject/l2e-contract",
      bitcointalk: "https://bitcointalk.org",
      "white-paper": "https://github.com/l2eproject/l2e-contract",
    },
  }

  constructor(props) {
    super(props)
  }

  open = (e) => {
    let {key} = e
    if (this.props.pages[key]) {
      Router.push(this.props.pages[key])
    }
    else if (this.props.links[key]) {
      window.open(this.props.links[key], "_blanck")
    }
    else { // using for test
      this.props.onClick(e)
    }
  }

  render() {
    return (
      <Header style={{
        position: 'fixed', zIndex: 10, width: '100%'
      }} >
        <div className="logo" style={{
          width: "120px",
          height: "31px",
          background: "rgba(255, 255, 255, .2)",
          margin: "16px 24px 16px 0",
          float: "left",
        }} > </div>
        <Menu
          theme="dark"
          mode="horizontal"
          selectable={false}
          style={{ lineHeight: '64px' }}
          onClick={this.open}
        >

          <Menu.Item key="index"> L2E </Menu.Item>
          <Menu.Item key="faq"> FAQ </Menu.Item>

          <Menu.SubMenu
            key="get-l2e"
            title={
              <span>
                <Icon type="down"/>
                <span>Get L2E</span>
              </span>
            }
          >
            <Menu.Item key="pre-sell">ETH Token Pre-Sell</Menu.Item>
            <Menu.Item key="Exchanges" disabled>Exchanges</Menu.Item>
          </Menu.SubMenu>
          <Menu.SubMenu
            key="resource"
            title={
              <span>
                <Icon type="down" />
                <span>Resource</span>
              </span>
            }
          >
            <Menu.Item key="github">Github</Menu.Item>
            <Menu.Item key="white-paper">White Paper</Menu.Item>
            <Menu.Item key="bitcointalk">Bitcointalk</Menu.Item>
            <Menu.Item key="mining-pool" disabled>Mining Pool</Menu.Item>
            <Menu.Item key="download" disabled>DownLoad</Menu.Item>
          </Menu.SubMenu>
        </Menu>
      </Header>
    )
  }
}