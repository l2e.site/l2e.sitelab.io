import React from 'react'

import Popover from 'antd/lib/popover'

export const Describe = ({ content, children }) =>
  <Popover
    placement="bottomLeft"
    content={content}
  >
    {children}
  </Popover>