import React from 'react';

import Layout from 'antd/lib/layout'
import Menu from 'antd/lib/menu'
import Icon from 'antd/lib/icon'
const { Sider } = Layout;

const CreateIcon = (icon) => {
  if (icon) return <Icon type={icon} />
}

const CreateSingleItem = ({ key, icon, name }) => 
  <Menu.Item key={key}>
    {CreateIcon(icon)}
    <span>{name}</span>
  </Menu.Item>

const CreateSubMenu = ({ key, icon, name, subs }) => 
  <Menu.SubMenu
    key={key}
    title={
      <span>
        <Icon type={icon} />
        <span>{name}</span>
      </span>
    }
  >
    {subs.map(CreateSingleItem)}
  </Menu.SubMenu>

const CreateItem = (e) => {
  return e.subs ? CreateSubMenu(e) : CreateSingleItem(e)
}

export default class SideBar extends React.Component {

  static defaultProps = {
    defaultKey: "chart",
    items: [
      {key: "chart", icon: "pie-chart", name: "Chart"},
      {
        key: "team", icon: "team", name: "Team",
        subs: [
          { key: "team1", name: "Team 1" },
        ]
      },
    ]
  }

  constructor(props) {
    super(props)
    this.state = {
      collapsed: false,
      // dynamic change the menu
      items: props.items,
    }
  }

  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  }

  render() {
    return (
      <Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
      >
        <div className="logo" style={{
          height: '32px',
          background: 'rgba(255,255,255,.2)',
          margin: '16px'
        }} />
        <Menu
          theme="dark"
          defaultSelectedKeys={[this.props.defaultKey]}
          mode="inline"
          onClick={this.props.onClick}
        >
          {
            this.state.items.map(CreateItem)
          }
        </Menu>
      </Sider>
    )
  }
}