import React from 'react';

import Radio from 'antd/lib/radio'
import LocaleProvider from "antd/lib/locale-provider";

import zhCN from 'antd/lib/locale-provider/zh_CN';
import moment from 'moment';
import 'moment/locale/zh-cn';

moment.locale('en');

export default class UnionApp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      locale: null,
    };
  }

  changeLocale = (e) => {
    const localeValue = e.target.value;
    this.setState({ locale: localeValue });
    if (!localeValue) {
      moment.locale('en');
    } else {
      moment.locale('zh-cn');
    }
  }

  render() {
    return(
      <div>
        <div className="change-locale">
          <Radio.Group defaultValue={undefined} onChange={this.changeLocale}>
            <Radio.Button key="en" value={undefined}>English</Radio.Button>
            <Radio.Button key="cn" value={zhCN}>中文</Radio.Button>
          </Radio.Group>
        </div>
        <LocaleProvider locale={this.state.locale}>
          {this.props.children}
        </LocaleProvider>
      </div>
    )
  }
}